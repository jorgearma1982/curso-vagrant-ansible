#!/usr/bin/env bash

apt-get update -qq
apt-get install -y -qq apache2 php5 postgresql-9.5
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi
